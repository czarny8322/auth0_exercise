# Compare active Rules

Json comparison program

## Getting Started

Program: compare.js - shows how we can get information about active rules
	There are two JSON string sources: rules_str, clients_str - You can change it or leave
	
Program: app.js - runs express server and shows active rules on website
	Tips: if you will see errors - Please refresh the page (F5) - it is still under construction

### Prerequisites


1. Please download all necessary modules
2. Fill your URL and TOKEN information under app.js file

```
npm install express fs request
```

