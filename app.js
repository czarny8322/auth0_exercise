const request = require('request')
const fs = require('fs')
const express = require('express')
const app = express()
const port = 3000

var url = 'PLEASE FILL YOUR URL HERE'
var rulesPath = '/api/v2/rules'
var clientsPath = '/api/v2/clients'

var token = 'PLEASE FILL YOUR TOKEN HERE'

var rules_f = 'rules.json'
var clients_f = 'clients.json'
var results_f = 'results.json'

app.get('/', (req, res) => {

var html = "No data" 
var results_obj 
  if (fs.existsSync(results_f) && fs.existsSync(rules_f) && fs.existsSync(rules_f)) {    
    results_obj = tryParse_str(fs.readFileSync(results_f, 'utf8'))
    html = buildHtml(req, results_obj); 
  }else{
    /// Call Function "saveJsonFiles" to save your Applicarions & Rules JSON string response to file/// 
    saveJsonFiles(rulesPath, clientsPath);      
    
    /// Compare two Json string to find relations between Aplication and Rule ///
    compareAndSaveResult(rules_f, clients_f);
  }
  res.end(html);
})


app.get('/download', (req, res) => {

  /// Call Function "saveJsonFiles" to save your Applicarions & Rules JSON string response to file/// 
  saveJsonFiles(rulesPath, clientsPath);   
  
  /// Compare two Json string to find relations between Aplication and Rule ///
  compareAndSaveResult(rules_f, clients_f);

  res.send("done");  
});

function compareAndSaveResult(rules_f, clients_f){
  
  var rules_str = fs.readFileSync(rules_f, 'utf8');
  var clients_str = fs.readFileSync(clients_f, 'utf8');
    
  var myResultsObj = compare(clients_str, rules_str)  

  // Save Finaly list in to file ///
  fs.writeFile(results_f, JSON.stringify(myResultsObj), function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("ArrayObject has been saved");
});
  
}

function saveJsonFiles(rulesPath, clientsPath){

  // GET AND SAVE JSON RULES ///
  getJsonFromAuth0(rulesPath, function(err, body) {
    if (err) {
      console.log(err.stack);
      return res.status(500);
    }    
    var body_str = JSON.stringify(body)
    fs.writeFile(rules_f, body_str, function (err) {
        if (err) throw err;
        console.log('Rules saved!');
      }); 
    //console.log(body)  
  }); 
  
  // GET AND SAVE JSON CLIENTS ///
  getJsonFromAuth0(clientsPath, function(err, body) {
    if (err) {
        console.log(err.stack);
        return res.status(500);
      }    
      var body_str = JSON.stringify(body)
      fs.writeFile(clients_f, body_str, function (err) {
          if (err) throw err;
          console.log('Clients saved!');
        }); 
      //console.log(body)    
  });
}

function getJsonFromAuth0(pathName, callback) {    
  request({
    method: 'GET',
    uri: url + pathName,
    headers: {'Authorization': 'Bearer ' + token},
    json:true
  }, function (err, res) {
    if (res.statusCode == 200) {
      var jsonList = res.body
      //callback(err, jsonList[0].name)
      callback(err, jsonList)
    } else {
      console.log("status code: " + res.statusCode)
      callback(err || ': Expected 200 status, received: ' + res.statusCode + '\n' + res.body);      
    }
  })
}

/////////////////////////////
function compare(clients_str, rules_str) {

  var clients_obj = tryParse_str(clients_str)
  var rules_obj = tryParse_str(rules_str)
  var applicationList = getAppList(clients_obj)

  resultsObjArr = compareAppRules(applicationList, rules_obj)

  return resultsObjArr
}

function compareAppRules(applicationList, rulesJsonObj) {

  var objestArr = [];
  var object = {
      Application: '',
      Rule: '',
      Match: ''
  };

  applicationList.forEach(appName => {
      object   = {
          Application: appName,
          Rule: "Not Found",
          Match: false
      }    

      rulesJsonObj.forEach(element => {
          var expectSrting = `if(context.clientName !== '${appName}')`;
          if (element.script.includes(expectSrting)) {
              object = {
                  Application: appName,
                  Rule: element.name,
                  Match: true
              }              
          }
      })
      objestArr.push(object);
  })
  return objestArr;
}

function getAppList(clients_obj) {
  var count = Object.keys(clients_obj).length;
  var myList = []
  if (count > 0) {
      clients_obj.forEach(element => {
          myList.push(element.name);
          //console.log(element.name); 
      });
  }
  return myList
}

function tryParse_str(string) {
  var jsonObj;
  if (string) {
      try {
          jsonObj = (JSON.parse(string));

          var count = Object.keys(jsonObj).length;
          console.log("succesfull parse string to JSON, objCount: " + count);
      } catch (e) {
          if (e instanceof SyntaxError) {
              printError(e, true);
          } else {
              printError(e, false);
          }
      }
  }
  return jsonObj
}

////////////////////////////

function buildHtml(req, results_obj) {
  var header = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>';
  var body = '';

  if(results_obj != null || Object.keys(results_obj).length>0){
      results_obj.forEach(element => {

          var color = '#ffffff'
          if (element.Match){
            color = '#009933'
          }

          body += "<tr bgcolor="+color+">" +            
                  "<th>" + element.Application + "</th>" +
                  "<th>" + element.Rule + "</th>" +
                  "<th>" + element.Match + "</th>" +            
                  "</tr>"
        }) 
  } else{
    body = "NO RESULTS" 
  }

  style = "<style>"+
          "table.roundedCorners {"+
          "border: 1px solid DarkOrange;"+
          "border-radius: 13px;" +
          "border-spacing: 0;}"+
          "table.roundedCorners td,"+
          "table.roundedCorners th {"+
          "border-bottom: 1px solid DarkOrange;"+
          "padding: 10px;}"+
          "table.roundedCorners tr:last-child > td {"+
          "border-bottom: none;}"+
          "</style>"


  headerTable = "<tr bgcolor=\"#6699ff\">"+
    "<th>Applications</th>"+
    "<th>Rule</th>"+
    "<th>IsMatch?</th>"+
  "</tr>"

  return '<!DOCTYPE html>'
       + '<html><head>' + header + '</head><body>'+style+'<table class="roundedCorners"><tbody>' + headerTable + body + '<tbody></table></body></html>'+
       '<center><button id=\'button1\'>Refresh</button></center>'+
       
       '<script>'+
       '$(\'#button1\').click(function(){'+
        'console.log(\'button clicked\');'+
        '$.ajax({url: \'download\', success:function(res){'+
            'console.log(\'server response is\', res);'+
            'window.location.reload()'+
        '}});'+
        
        '});'+
        '</script>'
};
app.listen(port, () => console.log(`Example app listening on port ${port}!`))