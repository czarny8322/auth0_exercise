var fs = require('fs');

var rules_f = 'rules.json';
var clients_f = 'clients.json';

//var rules_str = fs.readFileSync(rules_f, 'utf8');
//var clients_str = fs.readFileSync(clients_f, 'utf8');

///TEST SOURCE FILES////
var rules_str = "[{\"id\":\"rul_7F2RtL01c5t4Jvqc\",\"enabled\":true,\"script\":\"function (user, context, callback) {\\n\\n  \/\/ Roles should only be set to verified users.\\n  if (!user.email || !user.email_verified) {\\n    return callback(null, user, context);\\n  }\\n\\n  user.app_metadata = user.app_metadata || {};\\n  \/\/ You can add a Role based on what you want\\n  \/\/ In this case I check domain\\n  const addRolesToUser = function (user) {\\n    const endsWith = 'stawarz.allegro@gmail.com';\\nconsole.log(\\\"test\\\");\\n    if (user.email && (user.email === endsWith) && context.clientName === 'MyTestApp1') {\\n      console.log('admin');\\n      return [\\\"admin\\\"];\\n    }\\n    console.log('user');\\n    return [\\\"user\\\"];    \\n  };\\n\\n  const roles = addRolesToUser(user);\\n\\n  user.app_metadata.roles = roles;\\n  auth0.users.updateAppMetadata(user.user_id, user.app_metadata)\\n    .then(function () {\\n      context.idToken['https:\/\/example.com\/roles'] = user.app_metadata.roles;\\n      callback(null, user, context);\\n    })\\n    .catch(function (err) {\\n      callback(err);\\n    });\\n}\",\"name\":\"Set roles to a user\",\"order\":1,\"stage\":\"login_success\"},{\"id\":\"rul_RTXR6jQm0C7a4GvW\",\"enabled\":true,\"script\":\"function (user, context, callback) {\\n  const whitelist = ['1.2.3.4', '212.180.224.222']; \/\/ authorized IPs\\n  const userHasAccess = whitelist.some(function (ip) {\\n    return context.request.ip === ip;\\n  });\\n\\n  if (!userHasAccess) {\\n    return callback(new Error('Access denied from this IP address.'));\\n  }\\n\\n  return callback(null, user, context);\\n}\",\"name\":\"IP Address whitelist\",\"order\":2,\"stage\":\"login_success\"},{\"id\":\"rul_uC1GzeZ0sw74GZnX\",\"enabled\":true,\"script\":\"function (user, context, callback) {\\n\\n  \/\/ Access should only be granted to verified users.\\n  if (!user.email || !user.email_verified) {\\n    return callback(new UnauthorizedError('Access denied.'));\\n  }\\n\\n  \/\/ only enforce for NameOfTheAppWithWhiteList\\n  \/\/ bypass this rule for all other apps\\n  if(context.clientName !== 'MyTestApp'){\\n    return callback(null, user, context);\\n  }\\n\\n  const whitelist = [ 'user1@example.com', 'user2@example.com' ]; \/\/ authorized users\\n  const userHasAccess = whitelist.some(function (email) {\\n    return email === user.email;\\n  });\\n\\n  if (!userHasAccess) {\\n    return callback(new UnauthorizedError('Access denied.'));\\n  }\\n\\n  callback(null, user, context);\\n}\",\"name\":\"Whitelist for a Specific App\",\"order\":3,\"stage\":\"login_success\"},{\"id\":\"rul_JECdUMfpeXyDiUC5\",\"enabled\":true,\"script\":\"function (user, context, callback) {\\n\\n  \/\/ Access should only be granted to verified users.\\n  if (!user.email || !user.email_verified) {\\n    return callback(new UnauthorizedError('Access denied.'));\\n  }\\n\\n  \/\/ only enforce for NameOfTheAppWithWhiteList\\n  \/\/ bypass this rule for all other apps\\n  if(context.clientName !== 'NewApp'){\\n    return callback(null, user, context);\\n  }\\n\\n  const whitelist = [ 'user1@example.com', 'user2@example.com' ]; \/\/ authorized users\\n  const userHasAccess = whitelist.some(function (email) {\\n    return email === user.email;\\n  });\\n\\n  if (!userHasAccess) {\\n    return callback(new UnauthorizedError('Access denied.'));\\n  }\\n\\n  callback(null, user, context);\\n}\",\"name\":\"NEEEEW BEST RUULE\",\"order\":4,\"stage\":\"login_success\"}]"
var clients_str = "[{\"tenant\":\"dev6\",\"global\":true,\"callbacks\":[],\"is_first_party\":true,\"name\":\"All Applications\",\"owners\":[\"mr|google-oauth2|107993311430953219696\"],\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"gPJBDv8vxx8dzS6Z8SNxA2INKBJzzGQo\",\"callback_url_template\":false,\"client_secret\":\"XdDscl-UiWSF9iM-YVKr2wPEAFerz8gJPtbt2iUOkApvwo6EBkEVIRPjeLSGFSfE\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"client_aliases\":[],\"token_endpoint_auth_method\":\"client_secret_post\",\"app_type\":\"regular_web\",\"grant_types\":[\"authorization_code\",\"implicit\",\"refresh_token\",\"client_credentials\"],\"web_origins\":[],\"custom_login_page_on\":true},{\"tenant\":\"dev6\",\"global\":false,\"is_token_endpoint_ip_header_trusted\":false,\"name\":\"My App\",\"is_first_party\":true,\"oidc_conformant\":true,\"sso_disabled\":false,\"cross_origin_auth\":false,\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"sCPdtxGkqutUm44nJdaEuk1Vg7Giu0lt\",\"callback_url_template\":false,\"client_secret\":\"ZAPXwuGYt65KtG3kObo4flaPXmrgIAbWo7eHtUE-7fjmktTm5GOKD9CJ4cebM3b8\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"app_type\":\"regular_web\",\"grant_types\":[\"authorization_code\",\"implicit\",\"refresh_token\",\"client_credentials\"],\"custom_login_page_on\":true},{\"tenant\":\"dev6\",\"global\":false,\"is_token_endpoint_ip_header_trusted\":false,\"name\":\"MyTestApp1\",\"is_first_party\":true,\"oidc_conformant\":true,\"sso_disabled\":false,\"cross_origin_auth\":false,\"allowed_clients\":[],\"allowed_logout_urls\":[\"http:\/\/localhost:4000\"],\"callbacks\":[\"http:\/\/localhost:4000\/callback\"],\"native_social_login\":{\"apple\":{\"enabled\":false},\"facebook\":{\"enabled\":false}},\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"R2r7jZd9fc4WOgsohlsuDWW7By1JE5H1\",\"callback_url_template\":false,\"client_secret\":\"lDFcHRYMAIQ2QcxAonSEygFjDBKy-YIPLYXo10FxStNppAHP9f1eKfMiIML7DTH4\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"client_aliases\":[],\"token_endpoint_auth_method\":\"client_secret_post\",\"app_type\":\"regular_web\",\"grant_types\":[\"authorization_code\",\"implicit\",\"refresh_token\",\"client_credentials\"],\"custom_login_page_on\":true},{\"tenant\":\"dev6\",\"global\":false,\"is_token_endpoint_ip_header_trusted\":false,\"name\":\"Auth0 Management API (Test Application)\",\"is_first_party\":true,\"oidc_conformant\":true,\"sso_disabled\":false,\"cross_origin_auth\":false,\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"jpEDhsnZY0s0kHWMBbiJwj8zxQ5BFboU\",\"callback_url_template\":false,\"client_secret\":\"CuCfTue2PVdVde2L2jfB993gC_UYaervJw7mKdKVcnL4drVYGcy7880R-xO-jOa7\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"app_type\":\"non_interactive\",\"grant_types\":[\"client_credentials\"],\"custom_login_page_on\":true},{\"tenant\":\"dev6\",\"global\":false,\"is_token_endpoint_ip_header_trusted\":false,\"name\":\"API Explorer Application\",\"is_first_party\":true,\"oidc_conformant\":true,\"sso_disabled\":false,\"cross_origin_auth\":false,\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"2aXwDZE0A2npvh14D42qYbrqIwtGYuoa\",\"callback_url_template\":false,\"client_secret\":\"oY1vIIGa8JmOCu_ogM0cW7jPf3Hdra3Rm4PjQd2RhI7C7QEUEYGy6WR7ezLmEqlv\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"app_type\":\"non_interactive\",\"grant_types\":[\"client_credentials\"],\"custom_login_page_on\":true},{\"tenant\":\"dev6\",\"global\":false,\"is_token_endpoint_ip_header_trusted\":false,\"name\":\"NewApp\",\"is_first_party\":true,\"oidc_conformant\":true,\"sso_disabled\":false,\"cross_origin_auth\":false,\"signing_keys\":[{\"cert\":\"\",\"subject\":\"\/CN=dev6.eu.auth0.com\"}],\"client_id\":\"63PpN1umh6EldeXYKWYTTntfSq50y0Ax\",\"callback_url_template\":false,\"client_secret\":\"9V9WfgG9h7RFmvTTFcIj-bxVm5GkDf0Bsu4jjK9NW62PX1knpGx5c_vroPDq1cZ1\",\"jwt_configuration\":{\"alg\":\"RS256\",\"lifetime_in_seconds\":36000,\"secret_encoded\":false},\"app_type\":\"regular_web\",\"grant_types\":[\"authorization_code\",\"implicit\",\"refresh_token\",\"client_credentials\"],\"custom_login_page_on\":true}]";
///TEST SOURCE FILES////


/// Compare two Json string to find relations between Aplication and Rule
console.log(compare(clients_str, rules_str));

function compare(clients_str, rules_str) {

    var clients_obj = tryParse_str(clients_str)
    var rules_obj = tryParse_str(rules_str)
    var applicationList = getAppList(clients_obj)

    resultsObjArr = compareAppRules(applicationList, rules_obj)

    return resultsObjArr
}

function compareAppRules(applicationList, rulesJsonObj) {

  var objestArr = [];
  var object = {
      Application: '',
      Rule: '',
      Match: ''
  };

  applicationList.forEach(appName => {
      object   = {
          Application: appName,
          Rule: "Not Found",
          Match: false
      }    

      rulesJsonObj.forEach(element => {
          var expectSrting = `if(context.clientName !== '${appName}')`;
          if (element.script.includes(expectSrting)) {
              object = {
                  Application: appName,
                  Rule: element.name,
                  Match: true
              }              
          }
      })
      objestArr.push(object);
  })
  return objestArr;
}

function getAppList(clients_obj) {
    var count = Object.keys(clients_obj).length;
    var myList = []
    if (count > 0) {
        clients_obj.forEach(element => {
            myList.push(element.name);
            //console.log(element.name); 
        });
    }
    return myList
}

function tryParse_str(string) {
    var jsonObj;
    if (string) {
        try {
            jsonObj = (JSON.parse(string));

            var count = Object.keys(jsonObj).length;
            console.log("succesfull parse string to JSON, objCount: " + count);
        } catch (e) {
            if (e instanceof SyntaxError) {
                printError(e, true);
            } else {
                printError(e, false);
            }
        }
    }
    return jsonObj
}
